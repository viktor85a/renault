<?php

use common\widgets\DbText;
use yii\helpers\Url;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
?>

	<footer class="footer">
		<div class="container">

			<div class="row">

				<div class="col-lg-6 col-lg-offset-1 col-md-7 col-sm-7 col-xs-8">

					<div class="footer__copy">
						<p>QREACHERS. 2015. В качестве продолжения предыдущей статьи о мировых тенденциях в использовании мобильной рекламы</p>
					</div>

				</div>

				<div class="col-lg-2 col-lg-offset-3 col-md-3 col-md-offset-2 col-sm-4 col-sm-offset-1 col-xs-4 col-xs-offset-0">

					<div class="footer__logo">
                                            <a href="/"><img src="<?= Yii::getAlias('@frontendUrl/img/logo-gp.jpg') ?>" alt="#"></a>
					</div>

				</div>

			</div>

		</div><!--.container-->
	</footer>